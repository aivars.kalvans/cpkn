
//*****************************************************************************
//SIMPLE MODBUS I/O MODULE YURI GUSEV V1.2 ANALOG INPUT
//*****************************************************************************
#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_conf.h"
#include "stm32f10x_tim.h"
#include "stm32f10x_adc.h"

#include "misc.h"

#include "i2c.h"
#include "controller.h"



/***************************************************************************//**
 * Declare function prototypes
 ******************************************************************************/
void SetupClock(void);
void SetupUSART1(USART_InitTypeDef USART_InitStructure);
void SetupTIM2(void);
void SetupTIM7(void);
void SetupGPIO(void);
void Setup_ADC(void);
uint16_t Read_ADC1(uint8_t channel);

USART_InitTypeDef USART_InitStructure;
uint8_t run_loop;
//uint8_t inp[4],outp[4];

//***************************************************************************
//send data from uart1 if data is ready
//***************************************************************************
void net_tx3(UART_DATA *uart)
{
	//led PB15
			if(GPIO_ReadOutputDataBit  ( GPIOB,GPIO_Pin_15))
			GPIO_WriteBit(GPIOB,GPIO_Pin_15,Bit_RESET);
			else
			GPIO_WriteBit(GPIOB,GPIO_Pin_15,Bit_SET);

	if((uart->txlen>0)&(uart->txcnt==0))
  {
	    USART_ITConfig(USART1, USART_IT_RXNE, DISABLE);
	    USART_ITConfig(USART1, USART_IT_TC, ENABLE);

	    //rs485 txenable
	  	GPIO_WriteBit(GPIOA,GPIO_Pin_11,Bit_SET);
	  	USART_SendData(USART1, uart->buffer[uart->txcnt++]);
  }

}



//***************************************************************************
// *  USART1 interrupt
// **************************************************************************
void USART1_IRQHandler(void)
{
	//led PB14
		if(GPIO_ReadOutputDataBit  ( GPIOB,GPIO_Pin_14))
		GPIO_WriteBit(GPIOB,GPIO_Pin_14,Bit_RESET);
		else
		GPIO_WriteBit(GPIOB,GPIO_Pin_14,Bit_SET);

	//Receive Data register not empty interrupt
  	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
   {
  		 USART_ClearITPendingBit(USART1, USART_IT_RXNE);
  		uart1.rxtimer=0;

  			if(uart1.rxcnt>(BUF_SZ-2))
  			uart1.rxcnt=0;

  		 	uart1.buffer[uart1.rxcnt++]=USART_ReceiveData (USART1);


   }

  	//Transmission complete interrupt
    if(USART_GetITStatus(USART1, USART_IT_TC) != RESET)
  	{

  		USART_ClearITPendingBit(USART1, USART_IT_TC);
  		  if(uart1.txcnt<uart1.txlen)
  		{
  			USART_SendData(USART1,uart1.buffer[uart1.txcnt++]);
  		}
  		 else
  		{
  		 uart1.txlen=0;
  		 //rs485 tx disable
  		 GPIO_WriteBit(GPIOA,GPIO_Pin_11,Bit_RESET);

  		 USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
  		 USART_ITConfig(USART1, USART_IT_TC, DISABLE);
  		}
  	}

}

//***************************************************************************
//Timer interrupt
//***************************************************************************
void TIM2_IRQHandler(void)
{
	TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
	//led pa0
	if(GPIO_ReadOutputDataBit  ( GPIOA,GPIO_Pin_0))
	GPIO_WriteBit(GPIOA,GPIO_Pin_0,Bit_RESET);
	else
	GPIO_WriteBit(GPIOA,GPIO_Pin_0,Bit_SET);

	run_loop=1;


}
//************************************
void TIM7_IRQHandler(void)
{
	TIM_ClearITPendingBit(TIM7, TIM_IT_Update);

	//blinking with PB5
	if(GPIO_ReadOutputDataBit  ( GPIOB,GPIO_Pin_5))
	     GPIO_WriteBit(GPIOB,GPIO_Pin_5,Bit_RESET);
	else
		GPIO_WriteBit(GPIOB,GPIO_Pin_5,Bit_SET);

	   if((uart1.rxtimer++>uart1.delay)&(uart1.rxcnt>1))
	   uart1.rxgap=1;
	   else
	   uart1.rxgap=0;


}

//***************************************************************************
//main()
//***************************************************************************
int main(void)
{
	 uint16_t i;

	 //setting clock to hse 24Mhz
	SetupClock();


          //setting parametrs common for all uarts
	      USART_InitStructure.USART_BaudRate            = 9600;
          USART_InitStructure.USART_WordLength          = USART_WordLength_8b;
          USART_InitStructure.USART_StopBits            = USART_StopBits_1;
          USART_InitStructure.USART_Parity              = USART_Parity_No ;
          USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
          USART_InitStructure.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;

       //uarts inints+interrupts
       SetupUSART1(USART_InitStructure); //RS485

       //tim2+interrupts
       SetupTIM2();

       //tim7 +interrupts
       SetupTIM7();

       //RS485 TXE PIN , LED PINS  & i/o pins
       SetupGPIO();

       Setup_ADC();

       //timer 0.0001sec one symbol on 9600 ~1ms
       uart1.delay=30; //modbus gap 9600
      // uart1.delay=10; //modbus gap 38400

       //i2c configuration
      // I2C_Configuration();


       //cleaning values
       for(i=0;i<OBJ_SZ;i++)
       res_table.regs[i]=1;

     //reading eeprom
   	// for(i=0;i<OBJ_SZ*2;i++)
   	// res_table.bytes[i]=I2C_EE_ByteRead(i);

       //reading device adress only
       for(i=0;i<2;i++)
       res_table.bytes[i]=I2C_EE_ByteRead(i);

       SET_PAR[0]=5;//res_table.regs[0];//modbus address



     //Main loop
     while(1)
     {

    	 //i/o
    	 if(run_loop)
      {
    		 res_table.regs[1]= Read_ADC1(0);
    		 res_table.regs[2]= Read_ADC1(1);
    		 res_table.regs[3]= Read_ADC1(2);
    		 res_table.regs[4]= Read_ADC1(3);
    		 res_table.regs[5]= Read_ADC1(4);
    		 res_table.regs[6]= Read_ADC1(5);
    		 res_table.regs[7]= Read_ADC1(6);
    		 res_table.regs[8]= Read_ADC1(7);


    	 if(res_table.regs[9]) //PC0
    	 GPIO_WriteBit  ( GPIOC,GPIO_Pin_0,Bit_SET);
    	 else
    	 GPIO_WriteBit  ( GPIOC,GPIO_Pin_0,Bit_RESET);

    	 if(res_table.regs[10]) //PC1
    	 GPIO_WriteBit  ( GPIOC,GPIO_Pin_1,Bit_SET);
    	 else
    	 GPIO_WriteBit  ( GPIOC,GPIO_Pin_1,Bit_RESET);

    	 if(res_table.regs[11]) //PC2
    	 GPIO_WriteBit  ( GPIOC,GPIO_Pin_2,Bit_SET);
    	 else
    	 GPIO_WriteBit  ( GPIOC,GPIO_Pin_2,Bit_RESET);

    	 if(res_table.regs[12]) //PC3
    	 GPIO_WriteBit  ( GPIOC,GPIO_Pin_3,Bit_SET);
    	 else
    	 GPIO_WriteBit  ( GPIOC,GPIO_Pin_3,Bit_RESET);

    	 if(res_table.regs[13]) //PC4
    	 GPIO_WriteBit  ( GPIOC,GPIO_Pin_4,Bit_SET);
    	 else
    	 GPIO_WriteBit  ( GPIOC,GPIO_Pin_4,Bit_RESET);

    	 if(res_table.regs[14]) //PC5
    	 GPIO_WriteBit  ( GPIOC,GPIO_Pin_5,Bit_SET);
    	 else
    	 GPIO_WriteBit  ( GPIOC,GPIO_Pin_5,Bit_RESET);

    	 if(res_table.regs[15]) //PC6
    	 GPIO_WriteBit  ( GPIOC,GPIO_Pin_6,Bit_SET);
    	 else
    	 GPIO_WriteBit  ( GPIOC,GPIO_Pin_6,Bit_RESET);

    	 if(res_table.regs[16]) //PC7
    	 GPIO_WriteBit  ( GPIOC,GPIO_Pin_7,Bit_SET);
    	 else
    	 GPIO_WriteBit  ( GPIOC,GPIO_Pin_7,Bit_RESET);


    	 run_loop=0;
      }

    	//comms
    	if(uart1.rxgap==1)
    	 {
    		//led PA12
    					if(GPIO_ReadOutputDataBit  ( GPIOA,GPIO_Pin_12))
    					GPIO_WriteBit(GPIOA,GPIO_Pin_12,Bit_RESET);
    					else
    					GPIO_WriteBit(GPIOA,GPIO_Pin_12,Bit_SET);

     	 MODBUS_SLAVE(&uart1);
    	 net_tx3(&uart1);
    	 }

     }
}

/***************************************************************************//**
 * @brief Setup clocks
 ******************************************************************************/
void SetupClock()
{
      RCC_DeInit ();                    /* RCC system reset(for debug purpose)*/
      RCC_HSEConfig (RCC_HSE_ON);       /* Enable HSE                         */

      /* Wait till HSE is ready                                               */
      while (RCC_GetFlagStatus(RCC_FLAG_HSERDY) == RESET);

      RCC_HCLKConfig   (RCC_SYSCLK_Div1);   /* HCLK   = SYSCLK                */
      RCC_PCLK2Config  (RCC_HCLK_Div1);     /* PCLK2  = HCLK                  */
      RCC_PCLK1Config  (RCC_HCLK_Div2);     /* PCLK1  = HCLK/2                */
      RCC_ADCCLKConfig (RCC_PCLK2_Div4);    /* ADCCLK = PCLK2/4               */

      /* PLLCLK = 8MHz * 9 = 72 MHz                                           */
      RCC_PLLConfig (RCC_PLLSource_HSE_Div1, RCC_PLLMul_9);

      RCC_PLLCmd (ENABLE);                  /* Enable PLL                     */

      /* Wait till PLL is ready                                               */
      while (RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET);

      /* Select PLL as system clock source                                    */
      RCC_SYSCLKConfig (RCC_SYSCLKSource_PLLCLK);

      /* Wait till PLL is used as system clock source                         */
      while (RCC_GetSYSCLKSource() != 0x08);

      /* Enable USART1 and GPIOA clock                                        */
      RCC_APB2PeriphClockCmd (RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA, ENABLE);

}
//*************************************************************************************************
void SetupUSART1(USART_InitTypeDef USART_InitStructure)
{
	             NVIC_InitTypeDef  NVIC_InitStructure;
	             GPIO_InitTypeDef  GPIO_InitStructure;

	             /* Enable clock                                                   */
                 RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
                 RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
                 RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

                 /* Configure USART1 Rx (PA10) as input floating                         */
                 GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_10;
                 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
                 GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN_FLOATING;
                 GPIO_Init(GPIOA, &GPIO_InitStructure);

                 /* Configure USART1 Tx (PA09) as alternate function push-pull            */
                 GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_9;
                 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
                 GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
                 GPIO_Init(GPIOA, &GPIO_InitStructure);

            	  USART_Init(USART1, &USART_InitStructure);
              	  USART_Cmd(USART1, ENABLE);

              	  //Setting interrupts

                  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
                  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
                  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
                  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
                  NVIC_Init(&NVIC_InitStructure);

                  USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);  /* Enable Receive interrupt */

}

//*******************************************************

void SetupTIM2()
{
	NVIC_InitTypeDef  NVIC_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2 , ENABLE);
    TIM_DeInit(TIM2);

    //1 sec setup APB=36Mhz/(36*100)
    TIM_TimeBaseStructure.TIM_Prescaler= 72000;
    TIM_TimeBaseStructure.TIM_ClockDivision=TIM_CKD_DIV1;
    TIM_TimeBaseStructure.TIM_CounterMode=TIM_CounterMode_Up;
    TIM_TimeBaseStructure.TIM_Period=1000;//till what value timer will count

    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
    TIM_ClearFlag(TIM2, TIM_FLAG_Update);
    TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);
    TIM_Cmd(TIM2, ENABLE);


   // NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
      NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
      NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
      NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
      NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
      NVIC_Init(&NVIC_InitStructure);

      TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);
}


//*******************************************************
void SetupTIM7()
{
	NVIC_InitTypeDef  NVIC_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7 , ENABLE);
    TIM_DeInit(TIM7);

    //0.0001 sec setup APB=36Mhz/(36*100)
    TIM_TimeBaseStructure.TIM_Prescaler= 72;
    TIM_TimeBaseStructure.TIM_ClockDivision=TIM_CKD_DIV1;
    TIM_TimeBaseStructure.TIM_CounterMode=TIM_CounterMode_Up;
    TIM_TimeBaseStructure.TIM_Period=100;//till what value timer will count

    TIM_TimeBaseInit(TIM7, &TIM_TimeBaseStructure);
    TIM_ClearFlag(TIM7, TIM_FLAG_Update);
    TIM_ITConfig(TIM7,TIM_IT_Update,ENABLE);
    TIM_Cmd(TIM7, ENABLE);


   // NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
      NVIC_InitStructure.NVIC_IRQChannel = TIM7_IRQn;
      NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
      NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
      NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
      NVIC_Init(&NVIC_InitStructure);

      TIM_ITConfig(TIM7,TIM_IT_Update,ENABLE);
}

//***********************************************************************

void SetupGPIO(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

    /* Configure PA11 as rs485 tx select           */
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_11;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);


    // Configure PA0,PA12 led
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_0,GPIO_Pin_12;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    // Configure PB5,PB14,PB15 led
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_5|GPIO_Pin_14|GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOB, &GPIO_InitStructure);



    //pc0-7  outputs
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3|
    		GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_7;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOC, &GPIO_InitStructure);



    /* Configure pa1-8 inputs           */
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_1|GPIO_Pin_2||GPIO_Pin_3|GPIO_Pin_4|
    		GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_7|GPIO_Pin_8;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AIN;
    GPIO_Init(GPIOA, &GPIO_InitStructure);



}
//***************************************************************************

void Setup_ADC(void)
{
	ADC_InitTypeDef ADC_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

	/* ADC1 configuration ------------------------------------------------------*/
	    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	    ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	    ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	    ADC_InitStructure.ADC_NbrOfChannel = 8;
	    ADC_Init(ADC1, &ADC_InitStructure);

	    /* Enable ADC1 */
	    ADC_Cmd(ADC1, ENABLE);

	    /* Enable ADC1 reset calibaration register */
	    ADC_ResetCalibration(ADC1);
	    /* Check the end of ADC1 reset calibration register */
	    while (ADC_GetResetCalibrationStatus(ADC1));

	    /* Start ADC1 calibaration */
	    ADC_StartCalibration(ADC1);
	    /* Check the end of ADC1 calibration */
	    while (ADC_GetCalibrationStatus(ADC1));

}

//reading selected channel
uint16_t Read_ADC1(uint8_t channel)
{
  ADC_RegularChannelConfig(ADC1, channel, 1, ADC_SampleTime_41Cycles5);
  // Start the conversion
  ADC_SoftwareStartConvCmd(ADC1, ENABLE);
  // Wait until conversion completion
  while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
  // Get the conversion value
  return ADC_GetConversionValue(ADC1);
}


//********************************

